package android.cuevasm.crystalball;

import java.util.Random;

/**
 * Created by Student on 8/26/2015.
 */
public class Predictions {

    public static Predictions predictions;
    private String[] answers;

    private Predictions(){
        answers = new String[] {
                "Your wishes will come true, Dont let your dreams be dreams!",
                "Your wishes will NEVER come true, Let your dreams be dreams!"
        };

    }
    public static Predictions get(){
        if(predictions == null){
            predictions = new Predictions();
        }
        return predictions;
    }

    public String getPredictions()
    {
        int rnd = new Random().nextInt(answers.length);
        return answers[rnd];
    }
}
